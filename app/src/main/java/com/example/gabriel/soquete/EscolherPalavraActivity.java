package com.example.gabriel.soquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EscolherPalavraActivity extends AppCompatActivity {

    Button btnEscolher;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolher_palavra);

        btnEscolher = (Button) findViewById(R.id.btnEscolher);
        intent = new Intent(this, JogoForcaActivity.class);
        btnEscolher.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try
                {
                    startActivity(intent);
                }
                catch(Exception ex)
                {}

            }
        });
    }
}
