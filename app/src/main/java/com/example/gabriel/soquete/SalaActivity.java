package com.example.gabriel.soquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SalaActivity extends AppCompatActivity {
    
    Button btnComecarJogo;
    Intent intent;
    ConexaoSocket conn = ConexaoSocket.getInstance();
    TextView txtJogador;
    int nJog;
    LinearLayout linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sala);
        
        btnComecarJogo = (Button) findViewById(R.id.btnComecarJogo);
        intent = new Intent(this, EscolherPalavraActivity.class);
        linear = (LinearLayout) findViewById(R.id.layoutSala);

        btnComecarJogo.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try
                {
                    startActivity(intent);
                }
                catch(Exception ex)
                {}

            }
        });

        verificaNumeroJogadores();
    }

    private void verificaNumeroJogadores()
    {
        new Thread()
        {
            public void run()
            {

                try
                {

                    int a = 0;
                    int b;
                    while(true)
                    {
                        b = a;
                        try{
                            a = Integer.parseInt(conn.retornaMensagem());}
                        catch(Exception ex)
                        {}

                        nJog = a;
                        if(b != a)
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for (int i = 0; i < nJog; i++)
                                    {
                                        int resID = getResources().getIdentifier("txtJogador" + i, "id", getPackageName());
                                        txtJogador =  (TextView) findViewById(resID);
                                        txtJogador.setText("Jogador " + i);
                                    }

                                    if(nJog > 2)
                                    {
                                        btnComecarJogo.setEnabled(true);
                                    }

                                    linear.invalidate();
                                    linear.requestLayout();
                                }
                            });
                        }
                        Thread.sleep(200);

                    }

                }
                catch (Exception e)
                {
                    System.out.println(e.toString());
                }
            }

        }.start();

    }

    private void mostraJogadores(int nJogadoresConectados)
    {


    }

    private void liberaBotao()
    {

    }
}
