package com.example.gabriel.soquete;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ConexaoSocket
{
    private static ConexaoSocket instance;
    Socket s;
    DataInputStream din;
    DataOutputStream dout;
    public static ArrayList<String> listaMsgs = new ArrayList<>();

    public ConexaoSocket(String ip)
    {
        try
        {
            s = new Socket(ip, 4444);
            din = new DataInputStream(s.getInputStream());
            dout = new DataOutputStream(s.getOutputStream());
            recieveMsgFromServer();
        }
        catch(Exception ex)
        {

        }
    }

    public static synchronized ConexaoSocket getInstance(String ip) {
        if (instance == null)
            instance = new ConexaoSocket(ip);

        return instance;
    }

    public static synchronized ConexaoSocket getInstance() {
        return instance;
    }

    public void sendMsgToServer(String mensagem)
    {
        try
        {
            String msgout;
            msgout = mensagem;
            dout.writeUTF(msgout);
            dout.flush();
        }
        catch(Exception ex)
        {

        }
    }

    private void recieveMsgFromServer()
    {
        new Thread()
        {
            public void run()
            {
                try
                {
                    String msgin ="";
                    while(true)
                    {
                        msgin = din.readUTF();
                        listaMsgs.add(msgin);
                        sendMsgToServer("recebido: " + msgin);
                    }

                }
                catch (Exception e)
                {

                }

            }
        }.start();
    }

    public String retornaMensagem()
    {
        String retorno = "";
        if(listaMsgs.size() > 0)
        {
            retorno = listaMsgs.get(0);
            listaMsgs.remove(0);
        }

        return retorno;
    }
}
