package com.example.gabriel.soquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuPrincipalActivity extends AppCompatActivity {

    private Button btnEntrarSala;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        btnEntrarSala = (Button) findViewById(R.id.button3);
        intent = new Intent(this, SalaActivity.class);

        btnEntrarSala.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try
                {
                    ConexaoSocket conn = ConexaoSocket.getInstance();
                    conn.sendMsgToServer("jogador");
                    startActivity(intent);
                }
                catch(Exception ex)
                {}

            }
        });
    }
}
