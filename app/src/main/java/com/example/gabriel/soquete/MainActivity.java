package com.example.gabriel.soquete;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.jar.Manifest;
import java.io.ObjectOutputStream;
import java.io.*;
import java.net.*;

public class MainActivity extends AppCompatActivity {


    private EditText etMsg, etIp, etPort;
    public static EditText etResponse;
    private Button button;
    private String message;
    int port = 0;
    Intent intent;
    ConexaoSocket conn;


    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestPermissions(new String[]{android.Manifest.permission.INTERNET}, 1);
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE}, 2);
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_WIFI_STATE}, 3);
        requestPermissions(new String[]{android.Manifest.permission.CHANGE_WIFI_STATE}, 4);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent(this, MenuPrincipalActivity.class);
        etIp = (EditText) findViewById(R.id.editText);

        etMsg = (EditText) findViewById(R.id.editText3);

        button = (Button) findViewById(R.id.button);

        //StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        //.setThreadPolicy(policy);

        button.setOnClickListener(new View.OnClickListener()
        {
             public void onClick(View v)
            {
                try
                {
                    conn = ConexaoSocket.getInstance(etIp.getText().toString());
                    conn.sendMsgToServer(etMsg.getText().toString());
                    startActivity(intent);
                    //String msgout;
                    //msgout = etMsg.getText().toString();
                    //dout.writeUTF(msgout);
                    //dout.flush();

                }
                catch(Exception ex)
                {}

            }
        });

    }

}